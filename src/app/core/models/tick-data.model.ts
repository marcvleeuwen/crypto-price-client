import {Monetary} from './monetary.model';

export interface TickData {
  type: string;
  market: string;
  amount: Monetary;
  lastUpdate: number;
}

export interface TickDataFlat {
  exchange: string;
  fromCurrency: string;
  toCurrency: string;
  price: number;
  lastUpdate: number;
}
