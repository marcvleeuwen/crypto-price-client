import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TickMetaData} from '../../models/tick-meta-data.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MetaDataService {

  private readonly baseURl: string = '/meta-data';

  constructor(private readonly http: HttpClient) {
  }

  getTickMetaData(): Observable<TickMetaData> {
    return this.http.get<TickMetaData>(`${this.baseURl}/tick`);
  }
}
