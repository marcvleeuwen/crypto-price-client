import {TestBed} from '@angular/core/testing';

import {WebsocketService} from './websocket.service';
import {TickData} from '../../models/tick-data.model';

describe('WebsocketService', () => {
  let service: WebsocketService<TickData>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WebsocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
