import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {Observable, Subscriber} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService<T> {
  public socket: io.Socket;

  constructor() {
    if (environment.production) {
      this.socket = io();
    } else {
      this.socket = io('ws://localhost:3001');
    }
  }

  listen(eventName: string): Observable<T> {
    return new Observable((subscriber: Subscriber<T>) => {
      this.socket.on(eventName, (data: any) => {
        subscriber.next(data);
      });
    });
  }
}
