import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DataTableRow, HomeComponent} from './home.component';
import {WebsocketService} from '../../../core/services/websocket/websocket.service';
import {MetaDataService} from '../../../core/services/meta-data/meta-data.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {of} from 'rxjs';
import {TickMetaData} from '../../../core/models/tick-meta-data.model';
import {TickData, TickDataFlat} from '../../../core/models/tick-data.model';
import {MatChipsModule} from '@angular/material/chips';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let wss: WebsocketService<TickData>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        // Material
        MatTableModule,
        MatToolbarModule,
        MatProgressBarModule,
        MatChipsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule
      ],
      providers: [
        {
          provide: WebsocketService,
          useValue: {
            listen: () => of(TestData.mockTickData)
          }
        },
        {
          provide: MetaDataService,
          useValue: {
            getTickMetaData: () => of(TestData.mockTickMetaDataSingles)
          }
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    wss = TestBed.inject(WebsocketService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onInit', () => {
    it('should set the coins, exchanges and currencies', () => {
      //  Execute
      component.ngOnInit();
      //  Assert
      expect(component.coins).toEqual([{name: 'coin1', selected: true}]);
      expect(component.exchanges).toEqual([{name: 'exchange1', selected: true}]);
      expect(component.currencies).toEqual([{name: 'currency1', selected: true}]);
    });

    it('should flatten and transform TickData to a DataTableRow', () => {
      //  Execute
      component.ngOnInit();
      //  Assert
      expect(component.tickDataArr).toEqual([TestData.mockDataTableRow]);
    });

    describe('addOrUpdateTickList', () => {
      it('should set class text-red for a decreased price', () => {
        //  Prepare
        const baseTime = new Date(1602928683000);
        jasmine.clock().mockDate(baseTime);

        component.tickDataArr = [TestData.mockDataTableRow];
        spyOn(wss, 'listen').and.returnValue(of(TestData.mockTickDataPriceSmall));

        const expected = 'text-red';
        fixture.detectChanges();

        //  Execute
        component.ngOnInit();
        //  Assert
        expect(component.tickDataArr[0].class).toEqual(expected);

        jasmine.clock().uninstall();
      });

      it('should set class text-green for an increased price', () => {
        //  Prepare
        const baseTime = new Date(1602928683000);
        jasmine.clock().mockDate(baseTime);

        component.tickDataArr = [TestData.mockDataTableRow];
        spyOn(wss, 'listen').and.returnValue(of(TestData.mockTickDataPriceLarge));

        const expected = 'text-green';
        fixture.detectChanges();

        //  Execute
        component.ngOnInit();
        //  Assert
        expect(component.tickDataArr[0].class).toEqual(expected);

        jasmine.clock().uninstall();
      });

      it('should set class text-grey for an unchanged price', () => {
        //  Prepare
        const baseTime = new Date(1602928683000);
        jasmine.clock().mockDate(baseTime);

        component.tickDataArr = [TestData.mockDataTableRow];
        spyOn(wss, 'listen').and.returnValue(of(TestData.mockTickData));

        const expected = 'text-grey';
        fixture.detectChanges();

        //  Execute
        component.ngOnInit();
        //  Assert
        expect(component.tickDataArr[0].class).toEqual(expected);

        jasmine.clock().uninstall();
      });
    });
  });

  describe('onChipSelectionChange', () => {
    it('should filter not contain deselected coins', () => {
      //  Prepare
      component.tickDataArr = TestData.mockDataTableRowArray;
      fixture.detectChanges();

      //  Execute
      component.onChipSelectionChange('coins', 0, false);
      const filteredDataSource: Array<DataTableRow> = component.dataSource.data
        .filter((row) => row.toCurrency === component.coins[0].name);

      //  Assert
      expect(filteredDataSource.length).toEqual(0);
    });

    it('should filter not contain deselected exchanges', () => {
      //  Prepare
      component.tickDataArr = TestData.mockDataTableRowArray;
      fixture.detectChanges();

      //  Execute
      component.onChipSelectionChange('exchanges', 0, false);
      const filteredDataSource: Array<DataTableRow> = component.dataSource.data
        .filter((row) => row.toCurrency === component.exchanges[0].name);

      //  Assert
      expect(filteredDataSource.length).toEqual(0);
    });

    it('should filter not contain deselected currencies', () => {
      //  Prepare
      component.tickDataArr = TestData.mockDataTableRowArray;
      fixture.detectChanges();

      //  Execute
      component.onChipSelectionChange('currencies', 0, false);
      const filteredDataSource: Array<DataTableRow> = component.dataSource.data
        .filter((row) => row.toCurrency === component.currencies[0].name);

      //  Assert
      expect(filteredDataSource.length).toEqual(0);
    });
  });

  describe('applyFilter', () => {
    it('should ', () => {
      //  Prepare
      component.tickDataArr = TestData.mockDataTableRowArray;
      fixture.detectChanges();

      const filterValue = 'coin1';
      //  Execute
      component.applyFilter(filterValue);

      //  Assert
      expect(component.dataSource.filter).toEqual(filterValue);
    });
  });
});

class TestData {
  static mockTickMetaDataSingles: TickMetaData = {
    coins: ['coin1'],
    currencies: ['currency1'],
    exchanges: ['exchange1']
  };

  static mockTickMetaDataMultiple: TickMetaData = {
    coins: ['coin1', 'coin2', 'coin3'],
    currencies: ['currency1', 'currency2', 'currency3'],
    exchanges: ['exchange1', 'exchange2', 'exchange3']
  };

  static mockTickData: TickData = {
    type: '2',
    market: 'exchange1',
    amount: {
      price: 123.456,
      fromCurrency: 'coin1',
      toCurrency: 'currency1'
    },
    lastUpdate: 1602928686
  };

  static mockTickDataPriceLarge: TickData = {
    type: '2',
    market: 'exchange1',
    amount: {
      price: 999.999,
      fromCurrency: 'coin1',
      toCurrency: 'currency1'
    },
    lastUpdate: 1602928686
  };

  static mockTickDataPriceSmall: TickData = {
    type: '2',
    market: 'exchange1',
    amount: {
      price: -999.999,
      fromCurrency: 'coin1',
      toCurrency: 'currency1'
    },
    lastUpdate: 1602928686
  };

  static mockTickDataFlat: TickDataFlat = {
    exchange: 'exchange1',
    price: 123.456,
    fromCurrency: 'coin1',
    toCurrency: 'currency1',
    lastUpdate: 1602928686
  };

  static mockDataTableRow: DataTableRow = {
    exchange: 'exchange1',
    price: 123.456,
    fromCurrency: 'coin1',
    toCurrency: 'currency1',
    lastUpdate: 1602928686,
    class: ''
  };

  static mockDataTableRowArray: Array<DataTableRow> = [
    {
      exchange: 'exchange1',
      price: 123.456,
      fromCurrency: 'coin1',
      toCurrency: 'currency1',
      lastUpdate: 1602928686,
      class: ''
    }, {
      exchange: 'exchange2',
      price: 123.456,
      fromCurrency: 'coin2',
      toCurrency: 'currency2',
      lastUpdate: 1602928686,
      class: ''
    }, {
      exchange: 'exchange3',
      price: 123.456,
      fromCurrency: 'coin3',
      toCurrency: 'currency3',
      lastUpdate: 1602928686,
      class: ''
    }
  ];
}
