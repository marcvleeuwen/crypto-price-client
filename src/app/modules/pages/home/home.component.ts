import {Component, OnInit} from '@angular/core';
import {TickMetaData} from '../../../core/models/tick-meta-data.model';
import {WebsocketService} from '../../../core/services/websocket/websocket.service';
import {TickData, TickDataFlat} from '../../../core/models/tick-data.model';
import {MetaDataService} from '../../../core/services/meta-data/meta-data.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  loading = true;
  currencies: Array<{ name: string; selected: boolean }> = [];
  exchanges: Array<{ name: string; selected: boolean }> = [];
  coins: Array<{ name: string; selected: boolean }> = [];

  tickDataArr: Array<DataTableRow> = [];
  dataSource: MatTableDataSource<DataTableRow>;
  displayedColumns: string[] = ['exchange', 'fromCurrency', 'toCurrency', 'price', 'lastUpdate'];

  constructor(private readonly wssTick: WebsocketService<TickData>,
              private readonly metaData: MetaDataService) {

    this.dataSource = new MatTableDataSource();
  }

  private static flattenTickData(data: TickData): TickDataFlat {
    return {
      exchange: data.market,
      fromCurrency: data.amount.fromCurrency,
      toCurrency: data.amount.toCurrency,
      price: data.amount.price,
      lastUpdate: data.lastUpdate
    };
  }

  ngOnInit(): void {
    this.metaData.getTickMetaData()
      .subscribe((data: TickMetaData) => {
        // populate arrays used for the chips
        this.currencies = data.currencies.map((value: string) => {
          return {name: value, selected: true};
        });
        this.exchanges = data.exchanges.map((value: string) => {
          return {name: value, selected: true};
        });
        this.coins = data.coins.map((value: string) => {
          return {name: value, selected: true};
        });

        // Make connection to the Websocket service
        this.connectWssTick();
        this.loading = false;
      }, (e: Error) => {
        console.error(e);
        this.loading = false;
      });
  }

  onChipSelectionChange(grouping: 'coins' | 'currencies' | 'exchanges', index: number, selected: boolean): void {
    switch (grouping) {
      case 'currencies':
        this.currencies[index].selected = selected;
        break;
      case 'exchanges':
        this.exchanges[index].selected = selected;
        break;
      case 'coins':
        this.coins[index].selected = selected;
        break;
    }
    this.updateDataSource(this.tickDataArr);
  }

  applyFilter(value: string): void {
    this.dataSource.filter = value.trim().toLowerCase();
  }

  private connectWssTick(): void {
    this.wssTick.listen('coinsChanged')
      .subscribe((data: TickData) => {
        const flatData: TickDataFlat = HomeComponent.flattenTickData(data);
        this.addOrUpdateTickList(flatData);
      });
  }

  private addOrUpdateTickList(data: TickDataFlat): void {
    //  check if item exists
    const tickerDataArrKeys: Array<string> = this.tickDataArr.map((item: TickDataFlat) =>
      item.exchange
      + item.fromCurrency
      + item.toCurrency
    );
    const index: number = tickerDataArrKeys.indexOf(data.exchange + data.fromCurrency + data.toCurrency);
    if (!!data.price) {
      if (index > -1) {
        if (data.price > this.tickDataArr[index].price) {
          this.tickDataArr[index] = data;
          this.tickDataArr[index].class = 'text-green';
        } else if (data.price < this.tickDataArr[index].price) {
          this.tickDataArr[index] = data;
          this.tickDataArr[index].class = 'text-red';
        } else {
          this.tickDataArr[index] = data;
          this.tickDataArr[index].class = 'text-grey';
        }
      } else {
        this.tickDataArr.push(data);
      }

      this.updateDataSource(this.tickDataArr);
    }
  }

  private updateDataSource(data: Array<DataTableRow>): void {
    // filter data
    const coinsFiltered = this.coins.filter((coin) => coin.selected);
    const exchangesFiltered = this.exchanges.filter((exchange) => exchange.selected);
    const currenciesFiltered = this.currencies.filter((currency) => currency.selected);

    // Clean up classes for previous updates
    data.forEach((item: DataTableRow) => {
      if (item.lastUpdate < new Date().getTime() / 1000) {
        item.class = '';
      }
    });

    this.dataSource.data = data
      .filter((item: TickDataFlat) => coinsFiltered.some((coin) => coin.name === item.fromCurrency))
      .filter((item: TickDataFlat) => exchangesFiltered.some((exchange) => exchange.name === item.exchange))
      .filter((item: TickDataFlat) => currenciesFiltered.some((currency) => currency.name === item.toCurrency));
  }

}

// This is only used locally
export interface DataTableRow extends TickDataFlat {
  class?: string;
}
